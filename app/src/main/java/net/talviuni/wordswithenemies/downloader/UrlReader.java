package net.talviuni.wordswithenemies.downloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class UrlReader {
    public static List<String> loadUrl(final String url) throws UrlException {
        final URL actualUrl;
        try {
            actualUrl = new URL(url);
        } catch (MalformedURLException e) {
            throw new UrlException("Invalid url: " + url);
        }

        BufferedReader reader = null;
        final List<String> strings = new LinkedList<>();
        try {
            reader = new BufferedReader(new InputStreamReader(actualUrl.openStream()));
            String singleLine;
            while ((singleLine = reader.readLine()) != null) {
                strings.add(singleLine);
            }
        } catch (Exception e) {
            throw new UrlException("Loading failed: " + e.getMessage(), e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException uninterestingException) {
                    // Can't really do anything about this anymore.
                }
            }
        }
        return strings;
    }
}
