package net.talviuni.wordswithenemies.downloader;

public class UrlException extends Exception {
    public UrlException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UrlException(final String message) {
        super(message);
    }
}
