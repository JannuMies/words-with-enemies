package net.talviuni.wordswithenemies.downloader;

import android.os.AsyncTask;
import android.widget.TextView;

import net.talviuni.wordswithenemies.downloader.UrlException;
import net.talviuni.wordswithenemies.downloader.UrlReader;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * As far as I understand, this should not be an AsyncTask, since an UI redraw might interrupt it.
 * But that can be fixed later.
 */
public class UrlLoadTask extends AsyncTask<String, Integer, List<String>> {

    private final TextView statusText;
    private final List<String> loadedWords;

    public UrlLoadTask(final TextView statusText, List<String> loadedWords) {
        // TODO: Fix this and do it with Intents
        this.statusText = statusText;
        this.loadedWords = loadedWords;
    }

    private List<String> loadSingleUrl(final String url) {
        try {
            return UrlReader.loadUrl(url);
        } catch (UrlException e) {
            statusText.setText("URL loading failed: " + e.getMessage() + "\n cause: " + e.getCause());
            return null;
        }
    }

    @Override
    protected List<String> doInBackground(String... strings) {
        if (strings == null || strings.length == 0) {
            return new LinkedList<>();
        }
        final String firstUrl = strings[0];
        try {
            final List<String> partialResult = loadSingleUrl(firstUrl);
            if (partialResult == null) {
                return null;
            } else {
                return partialResult;
            }
        } catch (Exception e) {
            statusText.setText("Failure in text loading: " + e.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<String> strings) {
        if (strings != null) {
            statusText.setText("Loaded " + strings.size() + " unique strings.");
            loadedWords.addAll(strings);
        } else {
            statusText.setText("Something done fucked up...");
        }
    }
}
