package net.talviuni.wordswithenemies.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = UriEntity.class, version = 1)
public abstract class UriStore extends RoomDatabase {
    public abstract UriDao uriDao();
}
