
package net.talviuni.wordswithenemies.persistence;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;

import net.talviuni.wordswithenemies.Singletons;

import java.util.List;

public class FavouriteLinksLoader extends AsyncTask<Void, Integer, String> {

    private final EditText urlBox;
    private final Context context;

    public FavouriteLinksLoader(final EditText urlBox, final Context context) {
        // TODO: Fix this and do it with Intents
        this.urlBox = urlBox;
        this.context = context;
    }

    @Override
    protected String doInBackground(final Void... voids) {
        final UriDao uriStore = Singletons.getUriStore(context);
        final List<UriEntity> favourites = uriStore.getAll();
        if (favourites == null || favourites.isEmpty()) {
            return "Nothing saved. Specify an URI to load";
        } else {
            return favourites.get(0).getUri();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        urlBox.setText(s);
    }
}
