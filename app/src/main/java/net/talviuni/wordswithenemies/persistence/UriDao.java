package net.talviuni.wordswithenemies.persistence;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UriDao {
    @Query("SELECT * FROM urientity")
    List<UriEntity> getAll();

    @Insert
    void insertAll(UriEntity... users);

    @Delete
    void delete(UriEntity user);

}
