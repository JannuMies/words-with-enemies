package net.talviuni.wordswithenemies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.talviuni.wordswithenemies.downloader.UrlLoadTask;
import net.talviuni.wordswithenemies.persistence.FavouriteLinksLoader;
import net.talviuni.wordswithenemies.persistence.UriDao;
import net.talviuni.wordswithenemies.persistence.UriEntity;

import java.util.ArrayList;
import java.util.List;

public class WordDownloader extends AppCompatActivity {

    private EditText urlBox;
    private final List<String> loadedWords = new ArrayList<>();
    private WordDownloader dirtyHackSelfReference;
    private FavouriteLinksLoader favouriteLinksLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.dirtyHackSelfReference = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_woerds);
        urlBox = findViewById(R.id.sourceUrl);
        urlBox.setText("Loading from DB...");

        final TextView statusText = findViewById(R.id.statusText);

        final Button urlLoadButton = findViewById(R.id.urlButton);
        urlLoadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadedWords.clear();
                statusText.setText("Loading URL: " + urlBox.getText().toString());

                final UrlLoadTask urlLoadTask = new UrlLoadTask(statusText, loadedWords);
                urlLoadTask.execute(urlBox.getText().toString());
            }
        });

        final Button newWordsButton = findViewById(R.id.showWordsButton);

        newWordsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (loadedWords.isEmpty()) {
                    statusText.setText("Empty word list. Load some words.");
                    return;
                }

                final Intent wordIntent = new Intent(dirtyHackSelfReference, WordShower.class);
                wordIntent.putStringArrayListExtra("data", new ArrayList<>(loadedWords));
                startActivity(wordIntent);
            }
        });

        favouriteLinksLoader = new FavouriteLinksLoader(urlBox, getApplicationContext());
        favouriteLinksLoader.execute();
    }
}
