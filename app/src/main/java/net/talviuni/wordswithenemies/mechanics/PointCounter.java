package net.talviuni.wordswithenemies.mechanics;

import android.text.TextUtils;

public class PointCounter {
    private static final String SPACE = " ";
    private int correct = 0;
    private int pass = 0;
    private boolean active = true;

    public void correct() {
        if (!active) {
            return;
        }
        correct++;
    }

    public void pass() {
        if (!active) {
            return;
        }
        pass++;
    }

    @Override
    public String toString() {
        return "Correct: " + correct + ", passed: " + pass;
    }

    public void freeze() {
        this.active = false;
    }

    public String serialize() {
        final StringBuilder sb = new StringBuilder(correct);
        sb.append(correct).append(SPACE);
        sb.append(pass);
        return sb.toString();
    }

    public static PointCounter deserialize(final String saved) {
        if (saved == null || saved.isEmpty()) {
            return new PointCounter();
        }
        final String[] split = saved.trim().split(SPACE);
        if (split.length != 2) {
            return new PointCounter();
        }

        try {
            final int correct = Integer.parseInt(split[0]);
            final int pass = Integer.parseInt(split[1]);
            final PointCounter pointCounter = new PointCounter();
            pointCounter.correct = correct;
            pointCounter.pass = pass;
            return pointCounter;
        } catch (NumberFormatException e) {
            return new PointCounter();
        }
    }
}
