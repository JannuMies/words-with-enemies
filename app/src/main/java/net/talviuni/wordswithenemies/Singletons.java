package net.talviuni.wordswithenemies;

import android.arch.persistence.room.Room;
import android.content.Context;

import net.talviuni.wordswithenemies.persistence.UriDao;
import net.talviuni.wordswithenemies.persistence.UriStore;

public class Singletons {
    private static volatile UriStore uriStore;

    public static synchronized UriDao getUriStore(Context context) {
        if (uriStore == null) {
            uriStore = Room.databaseBuilder(context, UriStore.class, "words-with-enemies").build();
        }
        return uriStore.uriDao();
    }
}
