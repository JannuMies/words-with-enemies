package net.talviuni.wordswithenemies;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.talviuni.wordswithenemies.mechanics.PointCounter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static net.talviuni.wordswithenemies.R.id.correctButton;
import static net.talviuni.wordswithenemies.R.id.passButton;

public class WordShower extends AppCompatActivity {


    private List<String> words;
    private PointCounter pointCounter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_shower);
        words = getWords(savedInstanceState);
        pointCounter = getPointCounter(savedInstanceState);
        final TextView wordView = findViewById(R.id.wordView);
        final TextView pointView = findViewById(R.id.pointView);
        final Button correctButton = findViewById(R.id.correctButton);
        final Button passButton = findViewById(R.id.passButton);

        correctButton.setOnClickListener(buildClickListener(wordView, pointCounter, RoundResult.SUCCESS, pointView));
        passButton.setOnClickListener(buildClickListener(wordView, pointCounter, RoundResult.PASS, pointView));

        pointView.setText(pointCounter.toString());
        final Optional<String> firstWord = popRandomWord();
        if (firstWord.isPresent()) {
            wordView.setText(firstWord.get());
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {

    }

    private View.OnClickListener buildClickListener(final TextView wordView, final PointCounter pointCounter,
                                                    final RoundResult roundResult, final TextView pointView) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (roundResult) {
                    case PASS:
                        pointCounter.pass();
                        break;
                    case SUCCESS:
                        pointCounter.correct();
                        break;
                }
                final Optional<String> maybeString = popRandomWord();
                if (maybeString.isPresent()) {
                    wordView.setText(maybeString.get());
                } else {
                    pointCounter.freeze();
                    wordView.setText("== No more words ==");
                }
                pointView.setText(pointCounter.toString());
            }
        };
    }

    private Optional<String> popRandomWord() {
        if (words.isEmpty()) {
            return Optional.empty();
        }
        Collections.shuffle(words);
        final String result = words.get(0);
        words.remove(0);
        return Optional.of(result);
    }

    private List<String> getWords(final Bundle savedState) {
        if (savedState != null) {
            final ArrayList<String> savedData = savedState.getStringArrayList("savedData");
            if (savedData != null) {
                return savedData;
            }
        }
        if (getIntent() != null) {
            final ArrayList<String> intentData = getIntent().getStringArrayListExtra("data");
            if (intentData != null) {
                return intentData;
            }
        }
        return Collections.emptyList();
    }

    private PointCounter getPointCounter(final Bundle savedState) {
        if (savedState != null) {
            final String pointCounterSerialized = savedState.getString("pointCounter");
            return PointCounter.deserialize(pointCounterSerialized);
        }
        return new PointCounter();
    }

    private enum RoundResult {
        SUCCESS, PASS
    }
}
