package net.talviuni.wordswithenemies.mechanics;

import android.graphics.Point;

import org.junit.Before;
import org.junit.Test;

import static java.lang.String.format;
import static org.junit.Assert.*;

public class PointCounterTest {

    private static final String BASE_STRING = "Correct: %d, passed: %d";
    private PointCounter pointCounter;

    @Before
    public void setup() {
        pointCounter = new PointCounter();
    }

    @Test
    public void toString_shouldReturnZeroPoints_whenImmeadiatelyCalled() {
        // Act
        final String string = pointCounter.toString();

        // Assert
        assertEquals(format(BASE_STRING, 0, 0), string);
    }


    @Test
    public void serialize_shouldReturnZeroPoints_whenImmeadiatelyCalled() {
        // Act
        final String string = pointCounter.serialize();

        // Assert
        assertEquals("0 0", string);
    }

    @Test
    public void serialize_shouldReturnTwoSuccessesPoints_whenTwoSuccessesHaveBeenReported() {
        // Arrange
        pointCounter.correct();
        pointCounter.correct();

        // Act
        final String string = pointCounter.serialize();

        // Assert
        assertEquals("2 0", string);
    }


    @Test
    public void serialize_shouldReturnThreePasses_whenThreePassesHaveBeenReported() {
        // Arrange
        pointCounter.pass();
        pointCounter.pass();
        pointCounter.pass();

        // Act
        final String string = pointCounter.serialize();

        // Assert
        assertEquals("0 3", string);
    }

    @Test
    public void deserialize_shouldCreateEmptyCounter_withNullInput() {
        // Act
        final PointCounter newCounter = PointCounter.deserialize(null);

        // Assert
        assertEquals("0 0", newCounter.serialize());
    }

    @Test
    public void deserialize_shouldCreateEmptyCounter_withEmptyStringInput() {
        // Act
        final PointCounter newCounter = PointCounter.deserialize("");

        // Assert
        assertEquals("0 0", newCounter.serialize());
    }

    @Test
    public void deserialize_shouldCreateEmptyCounter_withSingleNumberInput() {
        // Act
        final PointCounter newCounter = PointCounter.deserialize("13");

        // Assert
        assertEquals("0 0", newCounter.serialize());
    }

    @Test
    public void deserialize_shouldCreateEmptyCounter_withSingleNoNumberInput() {
        // Act
        final PointCounter newCounter = PointCounter.deserialize("kebab");

        // Assert
        assertEquals("0 0", newCounter.serialize());
    }

    @Test
    public void deserialize_shouldCreateEmptyCounter_withThreeWordInput() {
        // Act
        final PointCounter newCounter = PointCounter.deserialize("iskender durum freiheit");

        // Assert
        assertEquals("0 0", newCounter.serialize());
    }

    @Test
    public void deserialize_shouldCreateEmptyCounter_withOneNumberAndOneLetterInput() {
        // Act
        final PointCounter newCounter = PointCounter.deserialize("jogurt 13");

        // Assert
        assertEquals("0 0", newCounter.serialize());
    }

    @Test
    public void deserialize_shouldCreateCorrectCounter_withActualInput() {
        // Act
        final PointCounter newCounter = PointCounter.deserialize("43 11");

        // Assert
        assertEquals("43 11", newCounter.serialize());
    }
}
