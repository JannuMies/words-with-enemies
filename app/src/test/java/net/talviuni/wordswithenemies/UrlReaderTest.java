package net.talviuni.wordswithenemies;

import net.talviuni.wordswithenemies.downloader.UrlReader;

import org.junit.Test;

import java.net.URL;
import java.util.List;

import static org.junit.Assert.*;

public class UrlReaderTest {


    @Test
    public void loadUrl_shouldReturnTextFileContents() throws Exception {
        // Setup
        final URL resource = this.getClass().getResource("/localfile.txt");
        assertNotNull("localfile.txt not found in resources", resource);

        // Call
        final List<String> strings = UrlReader.loadUrl(resource.toString());

        // Assert
        assertEquals(2, strings.size());
        assertEquals("foo", strings.get(0));
        assertEquals("bar", strings.get(1));
    }
}